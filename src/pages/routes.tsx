import React from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';

// global styles
import 'antd/dist/antd.css';
import 'assets/styles/style.css';

// pages
import Home from 'pages/Home';

import NotFound from 'pages/NotFound';

const Routes = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path='/' component={Home} />
                <Route path='*' component={NotFound} />
            </Switch>
        </BrowserRouter>
    );
};

export default Routes;
