import React, { PureComponent } from 'react';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import './style.css';

type Content = {
    type: 'square' | 'circle' | 'text' | 'block' | 'margin',
    sizes: {
        xs?: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24;
        sm?: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24;
        md?: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24;
        lg?: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24;
    },
    height: number,
};

interface IProps {
    classes?: any;
    content: Content[];
    spacing?: 8 | 16 | 24;
    style?: any;
}

interface IStates {
    sizes: Array<number>;
}

export default class Loading extends PureComponent<IProps, IStates> {
    public getMaxLines = (height: number) => {
        const line = {
            max: 100,
            min: 60,
        };

        const lineHeight = 28;
        const maxLines = Math.trunc(height / lineHeight);
        const sizes: Array<number> = [];

        for (let i = 0; i <= maxLines; i++) {
            let lineRandomSize = Math.floor(Math.random() * (line.max - line.min + 1) + line.min);

            if (sizes.includes(lineRandomSize) || lineRandomSize < line.min) {
                while (!sizes.includes(lineRandomSize) && lineRandomSize >= line.min) {
                    lineRandomSize = Math.floor(Math.random() * (line.max - line.min + 1) + line.min);
                }
            }

            sizes.push(lineRandomSize);
        }

        return sizes;
    }

    componentWillMount() {
        const { content } = this.props;

        Array.isArray(content) && content.map(el => {
            this.setState({ sizes: this.getMaxLines(el.height) });
        });
    }

    public render(): React.ReactNode {
        const { content, spacing, style } = this.props;
        const { sizes } = this.state;

        return (
            <div className='loading'>
                <Row gutter={spacing ? spacing : 16}>
                    {Array.isArray(content) && content.map((el, index) => {
                        return (
                            <Col
                                xs={el.sizes.xs}
                                sm={el.sizes.sm ? el.sizes.sm : null}
                                md={el.sizes.md ? el.sizes.md : null}
                                lg={el.sizes.lg ? el.sizes.lg : null}
                                key={`element-${index}`}
                            >
                                <div
                                    className={`${el.type != 'text' ? 'children' : ''} element-${el.type}`}
                                    id={`element-${el.type}-${index}`}
                                    style={{ ...style, height: el.height }}
                                >
                                    {el.type === 'text' && sizes.map((row, index) => <span key={'line-' + index} style={{ width: sizes[index] + '%' }}></span>)}
                                </div>
                            </Col>
                        );
                    })}
                </Row>
            </div>
        );
    }
}