import React from 'react';

export default class Icon extends React.PureComponent<any> {
    render() {
        const props = this.props;
        const keys = Object.keys(props);
        const classes = keys.map((k, i) => {
            return `${k}`;
        });

        return <i className={`${classes.join(' ')}`}></i>;
    }
}